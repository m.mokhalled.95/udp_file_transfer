CC = g++
CFLAGS = -lstdc++ 

ifeq ($(OS), Windows_NT)
	EXECUTABLE=transferUDP.exe
	CFLAGS = -lws2_32 -std=c++17 -D_WIN32
else
	EXECUTABLE=transferUDP
	CFLAGS = -lstdc++ 
endif



transferUDP: TransferUDP.cpp main.cpp
	$(CC)  -o $(EXECUTABLE) File.cpp TransferUDP.cpp main.cpp $(CFLAGS)

clean:
	rm -f *.o $(EXECUTABLE)
