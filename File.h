#ifndef __FILE_LIB__
#define __FILE_LIB__

#ifdef _WIN32
#include <windows.h>
#else
#include <unistd.h>
#include <fcntl.h>
#endif

#include <iostream>

#ifndef MAXCHARS
//maximum buffer size (for receive, send, read and write)
#define MAXCHARS 1024 //1KB
#endif

//defining types for opening file
typedef enum 
{
    OPEN_EXISTINGS = 0,  // Do not create. just open the file.
    CREATE_NEWS = 1,     // Do not open existing file. just create new if it doesn't exists.
    CREATE_OR_OPEN = 2  // If file exists open it, if it doesn't create new one.
} file_open_t;

typedef enum 
{
    READ,          // Open file just for write
    WRITE,         // Open file just for read
    READ_WRITE     // Open file with read and write permissions
} readwrite_t;


class File
{
    private:
    
    //the file properties for internal use

    #ifdef _WIN32
    //file object
    HANDLE file;
    // variable for number of read chars
    DWORD n; 
    #else
    //file object
    int file;
    // variable for number of read chars
    int n;
    #endif

    // type of file
    file_open_t open_type;

    //purpose of file. this will be set for security reason of files in programming.
    readwrite_t readwrite_type;

    public:

    //open an existing file or create a new file
    int create(char *address, file_open_t open_t, readwrite_t readwrite);

    //read the data of file to the buffer. returns number of red bytes.
    int get(char *buff, const int max_size);

    //write from buffer to the file. returns number of wrote bytes.
    int put(char *buff, const int bytes_to_write);

    //close the opened file
    int closeFile();
};

#endif
