# TransferUDP

**TrasnferUDP.h** and **TransferUDP.cpp** are two files which are defining the most important object of this program and its methods and properties.
This document will describe each related parts in both files, in a specific section.

## Windows vs Linux code
In Windows, some part of code is needed to be different with Linux. Specially, when the codes are written with native libraries because of more performance. For this purpose, there are a constant in each operating system which shows the type of it. 
For Windows, it is `_WIN32`(32-bit) or `_WIN64`(64-bit) and for Linux it is `__linux__`. These are pre-defined and you can separate your special with `#ifdef`, `#elif`, `#else` and `#endif`.
As the unix-like OSs (such as Linux, FreeBSD, Mac and etc) have the same fundamentals, most of libraries are the same for them. Because of that, the program is coded like this.

```C++
#ifdef _WIN32
// Windows code will be here
#else
// Linux code will be here
#endif

// common code will be here
```

## Libraries
The libraries are included to the **TransferUDP.h** file in two parts. One part is related to network and socket and the other part standard C/C++ libraries which were needed in this project. 
The socket libraries are different for Linux and Windows.

```C++
//socket libraries 
#ifdef _WIN32 //for windows
#include <Winsock2.h>
#include <windows.h>
#include <ws2tcpip.h>
#include <Ws2def.h>
#include <tchar.h>
#include <strsafe.h>
#include "mingw-std-threads/mingw.thread.h"
#else   // for unix-like OSs (Linux, BSDs and ...)
#include <sys/socket.h> 
#include <arpa/inet.h>
#include <netinet/in.h> 
#include <poll.h>
#include <thread>
#endif

//other library
#include <chrono>
#include <fcntl.h>
#include <sys/types.h> 
#include <string>
#include <strings.h>
#include <iostream>
#include <memory>
#include <unistd.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "File.h"
```
**Note:** "mingw-std-threads/mingw.thread.h" is a wrapper for thread library in windows. This library is a submodule.

**chrono** and **thread** libraries are added for sleep for a little time between each send.

## Global typedef and variable

Windows need the ws2_32 library which we add it. Windows also doesn't have bzero function which is added as a macro in the begin of TransferUDP.h file, after libraries.

```C++
#ifdef _WIN32
#pragma comment(lib, "ws2_32")

#define bzero(b,len) (memset((b), '\0', (len)), (void) 0) 
#endif
```

A typedef in this program shows the type(mode) of the object which can be *client* or *server*.
It is defined as an **typedef enum**. As defined by typedef, in the rest of the program, it can be used as a new variable.

```C++
//client or server
typedef enum
{
    server = false,
    client = true
}SocketType;
```

Another define is needed for the maximum amount of our buffers in the program. This can be changed in the different systems with different RAM, bandwidth and etc. These type of buffer are mostly used in the sending and receiving network, and reading and writing a file.

```C++
//maximum buffer size (for receive, send, read and write)
#define MAXCHARS 1024
```

## TransferUDP class
Like most of the classes it has two **private** and **public** parts.

### Private
This part is mostly configured and its properties.

```C++
private:
    //socket properties
    int sockfd; //socket object
    int n;
    struct sockaddr_in servaddr; //server IP address variable
    char buf[MAXCHARS];
    SocketType type;
```

**sockfd** is the Linux socket object.
**n** will be used in some part as a temporary variable in some parts
**servaddr** this variable shows the server IP address in the standard Linux socket type
**buf[MAXCHARS]** is a buffer for different situation such as to send, receive, read and write
**type** is the type of socket which can be client or server

Also for windows an initial socket code is needed which is like this.
```C++
        #ifdef _WIN32
        WSADATA mywsadata; //your wsadata struct, it will be filled by WSAStartup
        #endif
```

### Public
In this part, there are some functions with the main functionality for our purpose.

#### constructor (TransferUDP)

This is the first function which is been using when we create the object. Its definition in the **TransferUDP.h** is like this.

**Inputs:**

- **ip_address** is c_style string which contains server IP address.
- **port** is an int which contains port number of server.
- **typeUDP** can be server or client.

```C++
//constructor function which just set the internal variables and objects
TransferUDP(char* ip_address, int port, SocketType typeUDP);
```

Actually, this function will be declared when the object is created once and set the properties of the object.
This part of code in **TransferUDP.cpp**:

```C++
//constructor function of TransferUDP function
TransferUDP::TransferUDP(char* ip_address, int port, SocketType typeUDP)
{
    //initial the socket udp properties

    bzero(&servaddr, sizeof(servaddr)); //empty the variable
    
    servaddr.sin_family = AF_INET; //AF_INET for IPv4

    //set IP address
    servaddr.sin_addr.s_addr = inet_addr(ip_address); 
    //For binding to any IP address we can change uncomment the below line
    //servaddr.sin_addr.s_addr = INADDR_ANY;  
    
    //set port number in the correct format
    servaddr.sin_port = htons(port); 

    //private variable to define if it is client or server
    type = typeUDP;
}
```

After setting up network properties and defining the socket, IP and etc.

#### Config
This function duties are binding the IP address in server mode and giving feedback to the user in both client/server mode.

**Return:**

- It will return 0 if everything was OK and -1 if something went wrong.

Its declaration in **TransferUDP.h** is like this:

```C++
    //bind to the IP in server mode and originally giving feedback
    int config();
```

The code in **TransferUDP.cpp** is this.

```C++
//bind to the IP address in server mode and giving feedback through the program
int TransferUDP::config()
{
    //initial socket on windows
    #ifdef _WIN32
    if (WSAStartup(MAKEWORD(2,2),&mywsadata) != 0) //0x0202 refers to version of sockets we want to use.
    {
        cout << "\n\nWSAstartup error" << endl;
            exit(0);
    }
    #endif
    
    //create socket object
    /*  
        we used AF_INET for using IPv4
        and, SOCK_DGRAM for UDP
     */
    sockfd = socket(AF_INET, SOCK_DGRAM, 0); 

     //set receive timeout
    /*
        This part of code is because we need to exit after receiving a file.
        If after 1sec there was no input data, waiting for receiving packets will finish.

        NOTE: We can change this value of time if it need. Of course it is a big value.
     */
    struct timeval read_timeout;
    
    #ifdef _WIN32
    read_timeout.tv_sec = 100; //It's in millisecond in Windows
    #else
    read_timeout.tv_sec = 1;  //It's in second in Linux
    #endif

    read_timeout.tv_usec = 0;
    setsockopt(sockfd, SOL_SOCKET, SO_RCVTIMEO, (char *)&read_timeout, sizeof(read_timeout));


    if (type == server)
    {
        int result;
        result = bind(sockfd, (struct sockaddr *) &servaddr, sizeof(servaddr)); 
        if (result < 0) //if bindind has problem, result will be -1
        {
            cout << "\n\nserver can not bind. you may need to try as a root user." << endl;
            exit(0);
        }
        else //if server initial was OK   
        {
            cout << "server bind successfully." << result << endl;
        }
    }
    else //if we were in client mode
    {
        cout << "client mode.\r\n";
    }
    return 0;
}
```

First of all, this function initial sock2 library for Windows if the program is been compiling for Windows. This happens with `WSAStartup` function and if its return will be with error, the program will terminate with a message.

After it, the socket object will get value use `socket` function.

The next step is configuring the socket receiving timeout. As this socket is UDP, there is no timeout setting for connection. This timeout is used to find out the end of receiving a file. This means when sending end, after a little while receiving function will unblock the program and program be finished. 
The timeout `tv_sec` value in Windows is in millisecond and in Linux is in seconds.

After these, the function first checks **type** is **server**, try to bind the IP address. If the result was not ok it will return and exit the program with a message and if it was print a message and declare. In client mode, it just declares the mode.

#### sendFile
As its name says, we use it for sending a file. This sending will happen in client mode. The only input is file address because other inputs are set in the previous functions.

**Input:**

- **addr** is address of file which is exists and ready to send in c_style string type.

**Return:**

- It will return 0 if everything was OK.

```C++
//sending a file from address to the server in the client mode
        int sendFile(char* addr);
```

its procedure in the **TransferUDP.cpp** is like this.

```C++
int TransferUDP::sendFile(char* addr)
{
    char buf[MAXCHARS];
    int n;

    File file;
    file.create(addr, OPEN_EXISTINGS, READ);
    n = file.get(buf, MAXCHARS);

    while (n > 0) { //while file does not end
        std::this_thread::sleep_for(std::chrono::microseconds(10));
        //sending to server
        sendto(sockfd, buf, (int)n, 0, (struct sockaddr *) &servaddr, sizeof(servaddr));

        n = file.get(buf,MAXCHARS);
    }


    file.closeFile();
    return 0;
}
```

It first creates a local buffer. Then open the file. Then while the file is not ended, it will read to the buffer and send data to the server. After each sent, it has 10 microseconds delay, to get time to receiver for making file and writing previous buffer to it.

#### receiveFile

This function might become complicated sometimes but its main duty is creating a file and write the received data into it.

**Input:**

- **addrss** is address of file which is not exists and we want save file in it, in c_style string type.

**Return:**

- It will return 0 if everything was OK.

```C++
//receive a file when program is in server mode
int TransferUDP::receiveFile(char *address)
{
    char buf[MAXCHARS]; //buffer to storage data  
    int n = 0;

    //blocking program until first receive of data
    n = -1;
    while (n == -1) 
    {
        n = recvfrom(sockfd, buf, MAXCHARS, 0, NULL, NULL);
    }

    cout << "start " << n << endl;

    File file;
    file.create(address, CREATE_NEWS, WRITE);
    file.put(buf, n);

    //while there is data read and write in the file
    n = recvfrom(sockfd, buf, MAXCHARS, 0, NULL, NULL);
    while (n > 0) {
        file.put(buf, n);
        n = recvfrom(sockfd, buf, MAXCHARS+200, 0, NULL, NULL);
    }

    //close the file
    file.closeFile();
    
    cout << "The end" << endl;

    return 0;
}
```

The function will wait while there is no data to receive. By starting to receive first data, it will try to create a file. after opening try to write received data to the file. Then again will do that but without blocking. It means as soon as receive timeout overflows, the function will close the opened file and return.
