# main.cpp

## Overview

Final code and main code is in this file. The first step in this program is getting settings as input and process it. Then check if it is a server, try to receive a file and if it is a client try to send the file.

## Handle the inputs

This program gets its input arguments from the command line. A good way to handle these inputs is using **getopt.h** library. For this purpose, the first step is defining input names and options as a struct. As there is in the code we have 5 types of input: 
*help, mode, ip, port, and file.*

### define options

Style of define each of these must be like this.

```C++
{"option_long_name", requrire following arg/not(1/0), NULL, 'option_char'}
```

except help our other options need a following argument. At the end of options, we must define a NULL option like this.

```C++
{NULL, 0, NULL, 0}
```

and at the end define is like this.

```C++
//input arguments options list
const struct option long_options[] =
{
    {"help", 0, NULL, 'h'},
    {"mode", 1, NULL, 'm'},
    {"ip", 1, NULL, 'i'},
    {"port", 1, NULL, 'p'},
    {"file", 1, NULL, 'f'},
    {NULL, 0, NULL, 0}
};
```

### check_options()

Next step after getting the input is checking it and put it the right variable place. **check_options** function does this. Each item of the options will be checked as a case in this function, and if it has another following argument, the argument is in **optarg** variable. we can put it in our suitable varile.

This function is like This.

```C++
//This function checks if all inputs are available and organize them
int check_options(int argc, char* argv[]){
    int next_option;

    /* A string listing valid short options letters.  */
    const char* const short_options = "hm:i:p:f:";

    do {
        next_option = getopt_long (argc, argv, short_options,
                                long_options, NULL);

        switch (next_option)
        {
        case 'h':   /* -h or --help */
        print_help (stdout);
        break;
        
        case 'm':   /* -m or --mode */
        mode_str = optarg;
        cout << "mode is " << mode_str << endl;
        break;
        
        case 'i':   /* -i or --ip */
        ip = optarg;
        cout << "ip is " << ip << endl;
        break;

        case 'p':   /* -p or --port */
        port_str = optarg;
        cout << "port is " << port_str << endl;
        break;

        case 'f':   /* -f or --file */
        file_address = optarg;
        cout << "file is " << file_address << endl;
        break;
        
        case -1:    /* Done with options.  */
        break;

        default:    /* Something else: unexpected.  */
        abort ();
        }
    }
    while (next_option != -1);

    return 0;
}
```

### check_validity()

When the options are in the right variable we should check if the arguments are valid or not. This function will do that.
The other duty of this function is changing type of the variable from c style string to the suitable type.

**Return:**

- if everything was OK it will return 0, otherwise it will return -1.

```C++
//This function check if 
int check_validity(){
    
    //check IP validity
    if (!is_ip_valid(ip)) 
        return -1;

    //check if mode input is corect
    if (strcasecmp(mode_str, "client") == 0)
        { 
            mode = client;
            cout << "mode set client" << endl;
        }
    else if (strcasecmp(mode_str, "server") == 0)
        {
            mode = server;
            cout << "mode set server" << endl;
        }
    else
        return -1;

    //change port number from c style string to int
    port = atoi(port_str);
    if (port == 0)
        return -1;

    //if everything is ok it returns 0
    return 0;
    
}
```

## main() processes

As procedure is mentioned in overview. First of all we get inputs in **argc** and **argv** variables.

```C++
int main(int argc, char* argv[])
````

then check the options one by one(as explained in **check_options**).

```C++
check_options(argc, argv);
```

then check options validity and if it has problem return out the program.

```C++
if (check_validity() < 0)
    {
        cout << "there is some problem in the input parameters." << endl;
        return -1;
    }
```

After checking these options, **TransferUDP** object will be created and configured.

```C++
TransferUDP udp(ip, port, mode); //make udp object
udp.config(); 
```

and at the end the program will check if it is in server mode to receive a file and if it is in client mode, try to send the file.

```C++
TransferUDP udp(ip, port, mode); //make udp object
    udp.config();                    //configure it internally

    if (mode == server)
    {  
        udp.receiveFile(file_address);
    }
    else
    {
        udp.sendFile(file_address);
    }
```
