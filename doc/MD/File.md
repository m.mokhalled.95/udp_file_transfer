# File

**File.cpp** and **File.h** files are a cross-platform library for working with files. It uses open, read, write and close functions for Linux and CreateFile, ReadFile, WriteFile and CloseHandle functions for Windows.

## Libraries

As mentioned there are two types of libraries for Linux and Windows separately. **Windows.h" for windows which will be used if the program is complied in windows. The _WIN32 is defined in windows, by default.
**unistd.h** and **fcntl.h** are Linux libraries which are used in the program if it is not complied in Linux.

```C++
#ifdef _WIN32
#include <windows.h>
#else
#include <unistd.h>
#include <fcntl.h>
#endif

#include <iostream>
```

## Global Defines

There is **MAXCHARS** which shows maximum bytes in the buffers. Because if may be used in with TransferUDP library, it will be defined only if it is not defined before.

```C++
#ifndef MAXCHARS
//maximum buffer size (for receive, send, read and write)
#define MAXCHARS 1024 //1KB
#endif
```

### Opening File Types

There are two properties which it should be set when a file is created. First is about creating or opening file. The other is purpose of using file which could be for writing or reading. These options are defined in two types, in this library, **file_open_t** and **readwrite_t**.

```C++
//defining types for opening file
typedef enum 
{
    OPEN_EXISTINGS = 0,  // Do not create. just open the file.
    CREATE_NEWS = 1,     // Do not open existing file. just create new if it doesn't exists.
    CREATE_OR_OPEN = 2  // If file exists open it, if it doesn't create new one.
} file_open_t;

typedef enum
{
    READ,          // Open file just for write
    WRITE,         // Open file just for read
    READ_WRITE     // Open file with read and write permissions
} readwrite_t;
```

## File class

File class is the main class which is used for start working with files. It contains properties and function for using the file.

### Private

This part contains the file object and its type of properties.

#### The file object

This object is the main file in its native library. Its type is "Handle" in Windows and "int" in Linux. There is an "n" variable, too, which is for the number bytes read or write which has different types in Windows and Linux.

```C++
//the file properties for internal use

    #ifdef _WIN32
    //file object
    HANDLE file;
    // variable for number of read chars
    DWORD n; 
    #else
    //file object
    int file;
    // variable for number of read chars
    int n;
    #endif
```

#### File properties type

This shows the mentioned types of File for opening.

```C++
    // type of file
    file_open_t open_type;

    //purpose of file. this will be set for security reason of files in programming.
    readwrite_t readwrite_type;
```

### Public

Public part of the class is included some function to work with files.

------

#### create

This function will start working with file. It set properties and "file" variable. Then the other functions can work with the file.

**Inputs:**

- **address** is address of file to open or create which is a char*.
- **open_t** shows this file should be opened or created or both in the specific situation.
- **readwrite** is purpose of opening file, which is reading, writing or both.

**Return:**

- This function returns an int. If it fails, return value will be -1 and in the other situation it will be a value more than 0.

Its definition in _File.h_.

```C++
    //open an existing file or create a new file
    int create(char *address, file_open_t open_t, readwrite_t readwrite);
```

Its definition in _File.cpp_.

```C++
int File::create(char *address, file_open_t open_t, readwrite_t readwrite)
{
    // set the internal peroperties 
    open_type = open_t;
    readwrite_type = readwrite;


    #ifdef _WIN32
    int to, trw;
    switch (open_type)
    {
    case CREATE_NEWS:
        to = CREATE_NEW;
        break;
    
    case OPEN_EXISTINGS:
        to = OPEN_EXISTING;
        break;

    case CREATE_OR_OPEN:
        to = CREATE_ALWAYS;
        break;

    default:
        break;
    }

    switch (readwrite_type)
    {
    case READ:
        trw = GENERIC_READ;
        break;
    
    case WRITE:
        trw = GENERIC_WRITE;
        break;

    case READ_WRITE:
        trw = GENERIC_READ | GENERIC_WRITE;
        break;

    default:
        break;
    }

    file = CreateFile(address,                // name of the write
                       trw,          // open for writing
                       0,                      // do not share
                       NULL,                   // default security
                       to,             
                       FILE_ATTRIBUTE_NORMAL,  // normal file
                       NULL);                  // no attr. template

    if (file == INVALID_HANDLE_VALUE) 
    { 
        return -1;
    }

    return 3;
    #else
    // a variable for flags of opening
    int t;

    // convert input setting to Linux flag setting
    switch (open_type)
    {
    case CREATE_NEWS:
        t = O_CREAT | O_EXCL;
        break;
    
    case OPEN_EXISTINGS:
        t = 0;
        break;

    case CREATE_OR_OPEN:
        t = O_CREAT;
        break;

    default:
        break;
    }

    switch (readwrite_type)
    {
    case READ:
        t = t | O_RDONLY;
        break;
    
    case WRITE:
        t = t | O_WRONLY;
        break;

    case READ_WRITE:
        t = t | O_RDWR;
        break;

    default:
        break;
    }
    
    // create or open the file
    file = open(address, t, 0666);

    // if return was -1, it means there is an error here
    return file;
    #endif
}
```

------

#### get

This function duty is reading some byte from opened file.

**Inputs:**

- **buff** is pointer to a buffer of char.
- **max_size** is an in which shows max size of buffer and maximum bytes which can be read from the file.

**Return:**

- The return will be an int. If there was a problem, the value will be -1 otherwise, it will be the number of read bytes.

The function definition in _File.h_.

```C++
//read the data of file to the buffer. returns number of red bytes.
    int get(char *buff, const int max_size);
```

The function definition in _File.cpp_.

```C++
int File::get(char *buff, const int max_size)
{
    int n;
    // check if there is read permission.
    if (readwrite_type != READ && readwrite_type != READ_WRITE)
    {
        return -1;
    }
        
    #ifdef _WIN32
    DWORD lp;

    if( FALSE == ReadFile(file, buff, MAXCHARS, &lp, NULL))
    {
        printf("\n%d\n", GetLastError());
        return -1;
    }

    return (int)lp;
    #else
    
    // read maximum of max_size and return the number of red chars
    n = read(file, buff, max_size);

    return n;
    #endif
}
```

------

#### put

This function will write a buffer contents to the file.

**Inputs:**

- **buff** is pointer to a buffer of char.
- **bytes_to_write** is number of bytes from char file to write in the file.

**Return:**

- The return will be an int. If there was a problem, the value will be -1 otherwise, it will be the number of wrote bytes in the file.

The function definition in _File.h_.

```C++
    //write from buffer to the file. returns number of wrote bytes.
    int put(char *buff, const int bytes_to_write);
```

The function definition in _File.cpp_.

```C++
int File::put(char *buff, const int bytes_to_write)
{

    #ifdef _WIN32
    DWORD lp;
    bool res = WriteFile(  file,           // open file handle
                buff,      // start of data to write
                bytes_to_write,  // number of bytes to write
                &lp, // number of bytes that were written
                NULL);            // no overlapped structure
    if(!res)
        return -1;
    return (int)lp;
    #else
    // write and if there was some problem n will be -1
    int n;
    n = write(file, buff, bytes_to_write);
    return n;
    #endif
}
```

------

#### closeFile

After working on files, the file must be closed. This function will do that.

**Return:**

- It returns 0 if everything was OK.

The function definition in _File.h_.

```C++
    //close the opened file
    int closeFile();
```

The function definition in _File.cpp_.

```C++
int File::closeFile()
{
    //close the file
    #ifdef _WIN32
    CloseHandle(file);
    #else
    close(file);
    #endif

    return 0;
}
```
