#include "../File.h"
#include <iostream>

using namespace std;

int main()
{
    File file;

    int res;
    res = file.create("new.txt", CREATE_OR_OPEN, WRITE);
    cout << "create compelete " << res << endl;
    char *b = "hello123 world!123";
    res = file.put(b, 12);
    cout << "put compelete " << res << endl;
    file.closeFile();
    
    res = file.create("new.txt", OPEN_EXISTINGS, READ);
    cout << "create compelete " << res << endl;

    char bb[500];

    res = file.get(bb, 500);
    bb[res] = 0;
    cout << "get compelete " << res<< endl;
    cout << bb << endl;
    file.closeFile();

    return 0;
}