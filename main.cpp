#include "TransferUDP.h"
#include <getopt.h> //for manage the input arguments

using namespace std;

//global variables
char *ip, *mode_str, *port_str, *file_address; //an input argument first receive as a c style string
SocketType mode;
int port;

//input arguments options list
const struct option long_options[] =
{
    {"help", 0, NULL, 'h'},
    {"mode", 1, NULL, 'm'},
    {"ip", 1, NULL, 'i'},
    {"port", 1, NULL, 'p'},
    {"file", 1, NULL, 'f'},
    {NULL, 0, NULL, 0}
};

//This function prints a brief help if it needed
int print_help(FILE* stream)
{
    fprintf(stream, "Usage: ./transferUDP -m mode -ip server_ip_address -p port -f file_address \n\n\n");
    fprintf(stream, "-h --help \n\t Display this usage information.\n\n"
                    "-m --mode mode \n\t Mode can be client or server. Server will wait to receive a file.\n\n"
                    "-i --ip server_ip_address \n\t Server IP address must be mentioned in both client and server mode, because on server mode, \
                     the program need to bind it and  in client mode it is need as sending address.\n\n"
                    "-p --port port_number \n\t Again port number is related to server address, because of the reason mentioned in the previous information.\n\n"
                    "-f --file file_address \n\t This is important that which file is going to send in client mode and where to save received \
                     file in server mode. This address must be here.\n\r\n");

    return 0;
}

//This function checks if all inputs are available and organize them
int check_options(int argc, char* argv[]){
    int next_option;

    /* A string listing valid short options letters.  */
    const char* const short_options = "hm:i:p:f:";

    do {
        next_option = getopt_long (argc, argv, short_options,
                                long_options, NULL);

        switch (next_option)
        {
        case 'h':   /* -h or --help */
        print_help (stdout);
        break;
        
        case 'm':   /* -m or --mode */
        mode_str = optarg;
        cout << "mode is " << mode_str << endl;
        break;
        
        case 'i':   /* -i or --ip */
        ip = optarg;
        cout << "ip is " << ip << endl;
        break;

        case 'p':   /* -p or --port */
        port_str = optarg;
        cout << "port is " << port_str << endl;
        break;

        case 'f':   /* -f or --file */
        file_address = optarg;
        cout << "file is " << file_address << endl;
        break;
        
        case -1:    /* Done with options.  */
        break;

        default:    /* Something else: unexpected.  */
        abort ();
        }
    }
    while (next_option != -1);

    return 0;
}

//This function checks if an IP address which is in string format is valid
//This function could be in a seprate file as "tools.cpp" for example but now is here for more simplicity
bool is_ip_valid(char *ipAddress)
{
    struct sockaddr_in sa;
    #ifdef _WIN32   //in Windows
    int result = inet_addr(ipAddress);
    if (result == INADDR_NONE)
        result == -1;
    #else   //in Linux
    int result = inet_pton(AF_INET, ipAddress, &(sa.sin_addr));
    #endif
    return result != 0;
}

//This function check if all inputs are valid
int check_validity(){
    
    //check IP validity
    if (!is_ip_valid(ip)) 
        return -1;

    //check if mode input is corect
    if (strcasecmp(mode_str, "client") == 0)
        { 
            mode = client;
            cout << "mode set client" << endl;
        }
    else if (strcasecmp(mode_str, "server") == 0)
        {
            mode = server;
            cout << "mode set server" << endl;
        }
    else
        return -1;

    //change port number from c style string to int
    port = atoi(port_str);
    if (port == 0)
        return -1;

    //if everything is ok it returns 0
    return 0;
    
}

int main(int argc, char* argv[])
{
    check_options(argc, argv);
    if (check_validity() < 0)
    {
        cout << "there is some problem in the input parameters." << endl;
        return -1;
    }
        
    TransferUDP udp(ip, port, mode); //make udp object
    udp.config();                    //configure it internally

    if (mode == server)
    {  
        udp.receiveFile(file_address);
    }
    else
    {
        udp.sendFile(file_address);
    }
    
    return 0;
}