# Transfer file over UDP protocol

This project is a simple **UDP server/client** for sending and receiving files in **C++** and it works on the different Linux distributions. It is tested on **Manjaro Linux** desktop version with **kernel 4.19.49** and **x86_64** architecture, as well as, **ROS Lunar** which was installed as a container on **Docker**. 

**CHANGES:** Now, It works on **Windows**, too. This program has been compiled by **mingw** g++.

> **NOTE:** This project is written without large libraries such as boost. It is known that UDP protocol may have many lost packets depending on the network connection. Therefore, a number of checking protocols is needed to increase  more stability which may cause bad effects on the latency and speed.

## Changes

- some sleep added between each send, waiting for receiver to be ready.

- separated in another file for more readability.

- windows functions and libraries are added.

## TODO

- [ ] Add simple send an receive function in TransferUFP for easy error detection implementing
- [x] Update all documents
- [x] Recheck code comments

## How To Use

### 1. Install

Firstly, clone the project on your system.

```bash
git clone https://gitlab.com/m.mokhalled.95/udp_file_transfer.git
```

Go to the project directory.

```bash
cd udp_file_transfer
```

make the project.
**IN LINUX** g++ and cmake MUST be installed. 
**IN WINDOWS** mingw64 MUST be installed. The best way is to install [tdm-gcc](http://tdm-gcc.tdragon.net/download).

```bash
#in linux
make

#in windows (There is a compiled .exe file for x86_64. No need to do this step if it is on the same architecture.)
mingw32-make
```

now there is a file with the name of **transferUDP** in Linux or **transferUDP.exe** in Windows in the directory which is our program.

### 2. Use transferUDP file

For a brief help, you can use this command in the directory of file.

```bash
#in Linux
./transferUDP -h

#in Windows
transferUDP -h
```

Using this program must be in the following format.

```bash
#in Linux
./transferUDP -m mode -i server_IP_address -p server_port_number -f file_address

#in Windows
transferUDP -m mode -i server_IP_address -p server_port_number -f file_address
```

  - **-m** or **--mode** must be followed by "client" or "server" mode. In the server mode, the program will wait until a client sends its file data.
  
  - **-i** or **--ip** must be followed by the server IP address in both client and server mode. Because client need to find the server address and server needs to bind from that address.
  - **-p** or **--p** must be followed by server port number.
  - **-f** or **--file** in the client mode must show the original file location. In the server mode, it must show the destination file which it will receive and create.

> **NOTE:** Server mode usually need root access.

### 3. Example

For checking this program complete functionality we should use two commands.

**Server** can be created like this.

```bash
#in Linux
sudo ./transferUDP -m server -i 127.0.0.1 -p 255 -f rec.jpg

#in Windows
transferUDP -m server -i 127.0.0.1 -p 255 -f rec.jpg
```

This command will make a server which binds on 127.0.0.1:255 address. It will save the received file in the same dir by the name of "rec.jpg"

After that **Client** can be create like this.

```bash
#in Linux
./transferUDP -m client -i 127.0.0.1 -p 255 -f 1.jpg

#in Windows
transferUDP -m client -i 127.0.0.1 -p 255 -f 1.jpg
```

this command will send "1.jpg" file which is located in the same directory to the server with the mentioned IP address.
