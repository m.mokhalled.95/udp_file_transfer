#include "TransferUDP.h"

using namespace std;

//constructor function of TransferUDP function
TransferUDP::TransferUDP(char* ip_address, int port, SocketType typeUDP)
{
    //initial the socket udp properties

    bzero(&servaddr, sizeof(servaddr)); //empty the variable
    
    servaddr.sin_family = AF_INET; //AF_INET for IPv4

    //set IP address
    servaddr.sin_addr.s_addr = inet_addr(ip_address); 
    //For binding to any IP address we can change uncomment the below line
    //servaddr.sin_addr.s_addr = INADDR_ANY;  
    
    //set port number in the correct format
    servaddr.sin_port = htons(port); 

    //private variable to define if it is client or server
    type = typeUDP;
}

//bind to the IP address in server mode and giving feedback through the program
int TransferUDP::config()
{
    //initial socket on windows
    #ifdef _WIN32
    if (WSAStartup(MAKEWORD(2,2),&mywsadata) != 0) //0x0202 refers to version of sockets we want to use.
    {
        cout << "\n\nWSAstartup error" << endl;
            exit(0);
    }
    #endif
    
    //create socket object
    /*  
        we used AF_INET for using IPv4
        and, SOCK_DGRAM for UDP
     */
    sockfd = socket(AF_INET, SOCK_DGRAM, 0); 

     //set receive timeout
    /*
        This part of code is because we need to exit after receiving a file.
        If after 1sec there was no input data, waiting for receiving packets will finish.

        NOTE: We can change this value of time if it need. Of course it is a big value.
     */
    struct timeval read_timeout;
    
    #ifdef _WIN32
    read_timeout.tv_sec = 10; //It's in millisecond in Windows
    read_timeout.tv_usec = 0;
    #else
    read_timeout.tv_sec = 0;  //It's in second in Linux
    read_timeout.tv_usec = 10000;
    #endif

    setsockopt(sockfd, SOL_SOCKET, SO_RCVTIMEO, (char *)&read_timeout, sizeof(read_timeout));


    if (type == server)
    {
        int result;
        result = bind(sockfd, (struct sockaddr *) &servaddr, sizeof(servaddr)); 
        if (result < 0) //if bindind has problem, result will be -1
        {
            cout << "\n\nserver can not bind. you may need to try as a root user." << endl;
            exit(0);
        }
        else //if server initial was OK   
        {
            cout << "server bind successfully." << result << endl;
        }
    }
    else //if we were in client mode
    {
        cout << "client mode.\r\n";
    }
    return 0;
}



//Sending a file to the mentioned server when program is client
int TransferUDP::sendFile(char* addr)
{
    char buf[MAXCHARS];
    int n;

    File file;
    file.create(addr, OPEN_EXISTINGS, READ);

    //read data from file to buf
    n = file.get(buf, MAXCHARS);

    while (n > 0) { //while file does not end

        //delay for 10 microseconds after each send to wait for
        //receiver to be ready. (It would be deleted if there was an
        // error checking algorithm in the program.)
        std::this_thread::sleep_for(std::chrono::microseconds(10));
        
        //sending to server
        sendto(sockfd, buf, (int)n, 0, (struct sockaddr *) &servaddr, sizeof(servaddr));
        
        //read again
        n = file.get(buf,MAXCHARS);
    }


    file.closeFile();
    return 0;
}

//receive a file when program is in server mode
int TransferUDP::receiveFile(char *address)
{
    char buf[MAXCHARS]; //buffer to storage data  
    int n = 0;

    //blocking program until first receive of data
    n = -1;
    while (n == -1) 
    {
        n = recvfrom(sockfd, buf, MAXCHARS, 0, NULL, NULL);
    }

    cout << "start " << n << endl;

    File file;
    file.create(address, CREATE_NEWS, WRITE);
    
    //write to file from buf
    file.put(buf, n);

    //while there is data read and write in the file
    n = recvfrom(sockfd, buf, MAXCHARS, 0, NULL, NULL);
    while (n > 0) {
        //write to file the received data
        file.put(buf, n);
        
        //receive some data again
        n = recvfrom(sockfd, buf, MAXCHARS+200, 0, NULL, NULL);
    }

    //close the file
    file.closeFile();
    
    cout << "The end" << endl;

    return 0;
}