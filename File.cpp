#include "File.h"
//#include <iostream>

#include <stdio.h>
#include <errno.h>


int File::create(char *address, file_open_t open_t, readwrite_t readwrite)
{
    // set the internal peroperties 
    open_type = open_t;
    readwrite_type = readwrite;


    #ifdef _WIN32
    int to, trw;
    switch (open_type)
    {
    case CREATE_NEWS:
        to = CREATE_NEWS;
        break;
    
    case OPEN_EXISTINGS:
        to = OPEN_EXISTINGS;
        break;

    case CREATE_OR_OPEN:
        to = CREATE_ALWAYS;
        break;

    default:
        break;
    }

    switch (readwrite_type)
    {
    case READ:
        trw = GENERIC_READ;
        break;
    
    case WRITE:
        trw = GENERIC_WRITE;
        break;

    case READ_WRITE:
        trw = GENERIC_READ | GENERIC_WRITE;
        break;

    default:
        break;
    }

    file = CreateFile(address,                // name of the write
                       trw,          // open for writing
                       0,                      // do not share
                       NULL,                   // default security
                       to,             
                       FILE_ATTRIBUTE_NORMAL,  // normal file
                       NULL);                  // no attr. template

    if (file == INVALID_HANDLE_VALUE) 
    { 
        return -1;
    }

    return 3;
    #else
    // a variable for flags of opening
    int t;

    // convert input setting to Linux flag setting
    switch (open_type)
    {
    case CREATE_NEWS:
        t = O_CREAT | O_EXCL;
        break;
    
    case OPEN_EXISTINGS:
        t = 0;
        break;

    case CREATE_OR_OPEN:
        t = O_CREAT;
        break;

    default:
        break;
    }

    switch (readwrite_type)
    {
    case READ:
        t = t | O_RDONLY;
        break;
    
    case WRITE:
        t = t | O_WRONLY;
        break;

    case READ_WRITE:
        t = t | O_RDWR;
        break;

    default:
        break;
    }
    
    // create or open the file
    file = open(address, t, 0666);

    // if return was -1, it means there is an error here
    return file;
    #endif
}

int File::get(char *buff, const int max_size)
{
    int n;
    // check if there is read permission.
    if (readwrite_type != READ && readwrite_type != READ_WRITE)
    {
        return -1;
    }
        
    #ifdef _WIN32
    DWORD lp;

    if( FALSE == ReadFile(file, buff, MAXCHARS, &lp, NULL))
    {
        printf("\n%d\n", GetLastError());
        return -1;
    }

    return (int)lp;
    #else
    
    // read maximum of max_size and return the number of red chars
    n = read(file, buff, max_size);

    return n;
    #endif
} 

int File::put(char *buff, const int bytes_to_write)
{

    #ifdef _WIN32
    DWORD lp;
    bool res = WriteFile(  file,           // open file handle
                buff,      // start of data to write
                bytes_to_write,  // number of bytes to write
                &lp, // number of bytes that were written
                NULL);            // no overlapped structure
    if(!res)
        return -1;
    return (int)lp;
    #else
    // write and if there was some problem n will be -1
    int n;
    n = write(file, buff, bytes_to_write);
    return n;
    #endif
}

int File::closeFile()
{
    //close the file
    #ifdef _WIN32
    CloseHandle(file);
    #else
    close(file);
    #endif

    return 0;
}


