#ifndef __UDP_H__
#define __UDP_H__

//socket libraries 
#ifdef _WIN32 //for windows
#include <Winsock2.h>
#include <windows.h>
#include <ws2tcpip.h>
#include <Ws2def.h>
#include <tchar.h>
#include <strsafe.h>
#else   // for unix-like OSs (Linux, BSDs and ...)
#include <sys/socket.h> 
#include <arpa/inet.h>
#include <netinet/in.h> 
#include <poll.h>
#endif

//other library
#include <chrono>
#include <fcntl.h>
#include <sys/types.h> 
#include <string>
#include <strings.h>
#include <iostream>
#include <memory>
#include <unistd.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "File.h"
#include <thread>

#ifdef _WIN32
#pragma comment(lib, "ws2_32")

#define bzero(b,len) (memset((b), '\0', (len)), (void) 0) 
#endif

//client or server
typedef enum 
{
    server = false,
    client = true
}SocketType;

//maximum buffer size (for receive, send, read and write)
#define MAXCHARS 1024 //1KB

class TransferUDP
{
    private:
        //socket properties
        int sockfd; //socket object
        int n; 
        struct sockaddr_in servaddr; //server IP address variable
        char buf[MAXCHARS]; 
        SocketType type;

        #ifdef _WIN32
        WSADATA mywsadata; //your wsadata struct, it will be filled by WSAStartup
        #endif

    public:

        //constructor function which just set the internal variables and objects
        TransferUDP(char* ip_address, int port, SocketType typeUDP);

        //bind to the IP in server mode and originally giving feedback
        int config();

        //sending file from address to the server in the client mode
        int sendFile(char* addr);

        //receiving file and saving it in the address in server mode
        int receiveFile(char *address);

};

#endif